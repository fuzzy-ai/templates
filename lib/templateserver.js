// templateserver.js
// Copyright 2016-2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const Microservice = require('@fuzzy-ai/microservice')
const async = require('async')

const version = require('./version')
const HTTPError = require('./httperror')
const Template = require('./template')

class TemplateServer extends Microservice {
  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)
    return cfg
  }

  setupRoutes (exp) {
    exp.get('/templates', this.getTemplates)

    exp.get('/version', this.getAppVersion)
    exp.get('/live', this.dontLog, this.live)
    exp.get('/ready', this.dontLog, this.ready.bind(this))
  }

  getTemplates (req, res, next) {
    const templates = []
    const addTemplate = template => templates.push(template)
    Template.scan(addTemplate, (err) => {
      if (err) {
        return next(err)
      } else {
        return res.json(templates)
      }
    })
  }

  getAppVersion (req, res, next) {
    res.json({name: 'template', version})
  }

  live (req, res, next) {
    res.json({status: 'OK'})
  }

  ready (req, res, next) {
    if ((this.db == null)) {
      return next(new HTTPError('Database not connected', 500))
    }

    this.db.save('server-ready', 'template', 1, (err, saved) => {
      if (err != null) {
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }

  getSchema () {
    return {template: Template.schema}
  }

  startDatabase (callback) {
    super.startDatabase(err => {
      if (err) {
        return callback(err)
      } else {
        return this.ensureTemplates(err => callback(err))
      }
    })
  }

  ensureTemplates (callback) {
    const templates = require('./default')

    const ensureTemplate = (template, callback) =>
      Template.search({slug: template.slug}, (err, results) => {
        if (err) {
          return callback(err)
        } else if (results.length > 1) {
          return callback(new Error(`Too many templates for ${template.slug}`))
        } else if (results.length === 1) {
          return results[0].update(template, callback)
        } else {
          return Template.create(template, callback)
        }
      })

    return async.each(templates, ensureTemplate, callback)
  }
}

module.exports = TemplateServer
