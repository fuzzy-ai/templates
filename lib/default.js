// default.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

// Default Template data

module.exports = {
  tweets: {
    slug: 'tweet-relevance',
    name: 'Tweet relevance',
    description: 'Score tweets or other social media posts based on how relevant they are to a given user.',
    source: {
      outputs: {
        relevance: {
          veryLow: [0, 25],
          low: [0, 25, 50],
          medium: [25, 50, 75],
          high: [50, 75, 100],
          veryHigh: [75, 100]
        }
      },
      inputs: {
        'Number of likes': {
          veryLow: [0, 5],
          low: [0, 5, 10],
          medium: [5, 10, 50],
          high: [10, 50, 100],
          veryHigh: [50, 100]
        },
        'Number of shares': {
          veryLow: [0, 5],
          low: [0, 5, 10],
          medium: [5, 10, 50],
          high: [10, 50, 100],
          veryHigh: [50, 100]
        },
        'Age in minutes': {
          veryNew: [0, 60],
          new: [0, 60, 120],
          'recent': [60, 120, 720],
          'earlier today': [120, 720, 1440],
          'before today': [720, 1440]
        }
      },
      rules: [
        '[Number of likes] INCREASES relevance',
        '[Number of shares] INCREASES relevance',
        '[Age in minutes] DECREASES relevance'
      ]
    }
  },

  pricing: {
    slug: 'pricing',
    name: 'Dynamic pricing',
    description: 'Determine the optimal price to charge a user for a product or service.',
    source: {
      inputs: {
        'user recency': {
          'brand new': [0, 2],
          new: [0, 2, 4],
          recent: [2, 7, 14, 19],
          old: [14, 19, 26, 31],
          'very old': [26, 31]
        },
        'product sales per week': {
          'very low': [0, 5],
          low: [0, 5, 15, 20],
          medium: [15, 20, 45, 50],
          high: [45, 50, 70, 75],
          'very high': [70, 75]
        },
        'category sales per week': {
          'very low': [0, 25],
          low: [0, 25, 50, 75],
          medium: [50, 75, 175, 200],
          high: [175, 200, 475, 500],
          'very high': [475, 500]
        }
      },
      outputs: {
        discount: {
          'very low': [0, 5],
          low: [0, 5, 10],
          medium: [5, 10, 20, 25],
          high: [20, 25, 30, 35],
          'very high': [30, 40]
        }
      },
      rules: [
        'IF [user recency] IS [brand new] THEN discount IS high',
        'IF [user recency] IS new THEN discount IS medium',
        'IF [user recency] IS recent THEN discount IS low',
        'IF [user recency] IS old THEN discount IS [very low]',
        'IF [user recency] IS [very old] THEN discount IS [very low]',
        'IF [product sales per week] IS [very low] THEN discount IS [very high]',
        'IF [product sales per week] IS low THEN discount IS high',
        'IF [product sales per week] IS medium THEN discount IS medium',
        'IF [product sales per week] IS high THEN discount IS low',
        'IF [product sales per week] IS [very high] THEN discount IS [very low]',
        'IF [category sales per week] IS [very low] THEN discount IS [very high]',
        'IF [category sales per week] IS low THEN discount IS high',
        'IF [category sales per week] IS medium THEN discount IS medium',
        'IF [category sales per week] IS high THEN discount IS low',
        'IF [category sales per week] IS [very high] THEN discount IS [very low]'
      ]
    }
  }
}
