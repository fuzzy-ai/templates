// agenttemplate.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash')
const CSON = require('cson')

const {DatabankObject} = require('databank')

const AgentTemplate = DatabankObject.subClass('agenttemplate')

AgentTemplate.schema = {
  pkey: 'slug',
  fields: [
    'slug',
    'name',
    'description',
    'template',
    'createdAt',
    'updatedAt'
  ],
  indices: ['slug']
}

AgentTemplate.newAgent = params => {
  const cson = _.template(this.source)
  return CSON.parseString(cson(params))
}

module.exports = AgentTemplate
