# templates
Agent Templates endpoint for fuzzy.ai

## Endpoints

* `GET /templates`: Fetch a list of agent templates.
* `GET /live`: Liveness probe. Returns `{status: "OK"}` if server is up.
* `GET /ready`: Readiness probe. Returns `{status: "OK"}` if server is up *and*
  can connect to the database.

## Authorization

Uses standard fuzzy.ai-microservice `APP_KEY` authorization for `/templates`.
