FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/templates
COPY . /opt/templates

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/templates/bin/dumb-init", "--"]
CMD ["npm", "start"]
