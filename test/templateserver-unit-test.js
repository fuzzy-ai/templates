// templateserver-test.js
// Copyright 2014,2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('template server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const TemplateServer = require('../lib/templateserver')
          callback(null, TemplateServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, TemplateServer) {
        return assert.ifError(err)
      },
      'it is a class' (err, TemplateServer) {
        assert.ifError(err)
        return assert.isFunction(TemplateServer)
      },
      'and we instantiate a TemplateServer': {
        topic (TemplateServer) {
          const { callback } = this
          try {
            const env = {
              PORT: '2342',
              HOSTNAME: 'localhost',
              DRIVER: 'memory',
              LOG_FILE: '/dev/null'
            }
            const server = new TemplateServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          return assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          return assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            server.start((err) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            return assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  body = JSON.parse(body)
                  return callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, version) {
              return assert.ifError(err)
            },
            'it looks correct' (err, version) {
              assert.ifError(err)
              assert.include(version, 'version')
              return assert.include(version, 'name')
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    return callback(err)
                  } else {
                    return callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                return assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
