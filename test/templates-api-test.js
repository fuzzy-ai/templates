// templates-api-test.js
// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./api-batch')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('GET /templates')
  .addBatch(apiBatch({
    'and we request /templates': {
      topic () {
        request.get('http://localhost:2342/templates', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        return assert.isString(body)
      },
      'and we parse the body': {
        topic (response, body) {
          return JSON.parse(body)
        },
        'it works' (err, templates) {
          return assert.ifError(err)
        },
        'and we examine the parsed JSON': {
          topic (templates) {
            return templates
          },
          'it is an array' (err, templates) {
            assert.ifError(err)
            return assert.isArray(templates)
          },
          'each element is a valid template' (err, templates) {
            assert.ifError(err)
            assert.isArray(templates)
            return (() => {
              const result = []
              for (const template of Array.from(templates)) {
                assert.isObject(template)
                assert.isString(template.slug)
                assert.isString(template.name)
                assert.isString(template.description)
                assert.isObject(template.source)
                assert.isObject(template.source.inputs)
                assert.isObject(template.source.outputs)
                result.push(assert.isArray(template.source.rules))
              }
              return result
            })()
          }
        }
      }
    }
  })).export(module)
