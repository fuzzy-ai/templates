// ready-probe-test.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const apiBatch = require('./api-batch')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('GET /ready')
  .addBatch(apiBatch({
    'and we request /ready': {
      topic () {
        request.get('http://localhost:2342/ready', this.callback)
        return undefined
      },
      'it works' (err, response, body) {
        assert.ifError(err)
        assert.isObject(response)
        assert.equal(response.statusCode, 200)
        assert.isString(body)
        return assert.equal(JSON.parse(body).status, 'OK')
      }
    }
  })).export(module)
