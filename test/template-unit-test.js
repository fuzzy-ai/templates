// template-test.js
// Copyright 2014,2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const {Databank, DatabankObject} = require('databank')

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('Agent Template data type')
  .addBatch({
    'When we load the template module': {
      topic () {
        try {
          const Template = require('../lib/template')
          this.callback(null, Template)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, Template) {
        assert.ifError(err)
        return assert.isFunction(Template)
      },
      'and we set up the database': {
        topic (Template) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {user: Template.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err, null)
            } else {
              DatabankObject.bank = db
              return callback(null, db)
            }
          })
          return undefined
        },
        'it works' (err, db) {
          assert.ifError(err)
          return assert.isObject(db)
        },
        'teardown' (db) {
          const { callback } = this
          if (db != null) {
            db.disconnect(err => {
              if (err) {
                console.error(err)
              }
              callback(null)
            })
          } else {
            callback(null)
          }
          return undefined
        },
        'and we create a new Template': {
          topic (db, Template) {
            const { callback } = this
            const props = {
              slug: 'unit_test',
              name: 'Unit test',
              description: 'Unit Test Agent',
              template: `\
name: <%= name %>\
`
            }
            return Template.create(props, (err, template) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null, template)
              }
            })
          },
          'it works' (err, template) {
            assert.ifError(err)
            assert.isObject(template)
            assert.isString(template.slug)
            assert.isString(template.name)
            return assert.isString(template.description)
          },
          'and we wait 2 seconds': {
            topic (template) {
              const { callback } = this
              const wait = () => callback(null)
              setTimeout(wait, 2000)
              return undefined
            },
            'it works' (err) {
              return assert.ifError(err)
            },
            'and we update the template': {
              topic (template) {
                template.update({apiLimit: 500000}, this.callback)
                return undefined
              },
              'it works' (err, template) {
                assert.ifError(err)
                assert.isObject(template)
                return assert.isString(template.slug)
              }
            }
          }
        }
      }
    }}).export(module)
