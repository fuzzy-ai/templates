// api-batch.js
// Does boilerplate for API tests
// Copyright 2017 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const _ = require('lodash')
const vows = require('perjury')
const { assert } = vows
const waitForPort = require('wait-for-port')

module.exports = function (rest) {
  const base = {
    'When we wait for the server': {
      topic () {
        waitForPort('localhost', 2342, this.callback)
      },
      'it works' (err) {
        assert.ifError(err)
      }
    }
  }

  _.extend(base['When we wait for the server'], rest)

  return base
}
